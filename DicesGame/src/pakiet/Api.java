package pakiet;

import java.util.Scanner;

public class Api {
	Generator gen = new Generator();
	
	int [] opcje = {0,1,2};

	private Scanner czytaj;
	
	Api(){
		int menu;
		do{
			System.out.println("Wybierz losowanie:");
			System.out.println("----------------------------");
			System.out.println("1) Pozycje - Miejsca");
			System.out.println("2) Co - Gdzie");
			System.out.println("0) Wyjd�");
			
			menu = pobierzWartosc();
			
			switch(menu){
			case 1:
				gen.losujKombinacje(1);
				break;
			case 2:
				gen.losujKombinacje(2);
				break;
			}
			
		}while(menu!=0);
	}
	
	int pobierzWartosc(){
		czytaj = new Scanner(System.in);
		boolean poprawnaPozycja = false;
		int menu;
		
		do{
			System.out.print(">> ");
			menu=czytaj.nextInt();
			
			for (int i=0; i<opcje.length; i++){
				if (opcje[i]==menu) poprawnaPozycja = true;
			}
		}while(poprawnaPozycja==false);
		
		return menu; 
		
	}
}
